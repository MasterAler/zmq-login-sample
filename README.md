# ZMQ Login Sample

Minimal sample from working project, implementing client-server interaction via ZMQ.


## What's inside?
Can login into empty window, getting user list from DB and checking password on server.

* Qt Client
* Python Server
* ZMQ transport
* PostgreSQL database

DB configuration:

* project\nozzle\datasource\dbsource\dbsource\install.sql is a lame but valid dump of example db (tables users & pilots needed only)
* connection string is DB_CONNECTION_STRING in \project\nozzle\datasource\dbsource\dbsource\config.py

ZMQ if configured to listen to localhost:43000.
Python dependencies: zmq, nose (if you need tests).

Blame author if something goes wrong or just ask him, why could that happen.
