#include "Loader.h"

#include "RequestContext.h"

#include "EntryPoint.h"
#include "MainView.h"

#include "PluginInterface.h"
#include "SubviewWidget.h"

#include <QDir>
#include <QCoreApplication>
#include <QPluginLoader>

#include <QDebug>

namespace
{
    const QString SUBVIEWS_DIR("subview");

#ifdef Q_OS_LINUX
    const QString SHARED_LIBRARY_SUFFIX = QStringLiteral("*.so*");
#else
    const QString SHARED_LIBRARY_SUFFIX = QStringLiteral("*.dll");
#endif
}

Loader::Loader(QObject* parent)
    : QObject(parent)
    , m_requestContext(new RequestContext)
    , m_mainView(new MainView)
    , m_state(EntryPoint::NotAuthenticated)
{}

Loader::~Loader()
{
    foreach(const QString& key, m_subviews.keys())
        if (m_subviews[key]->parent() == nullptr)
            delete m_subviews[key];
}

bool Loader::authDialogAccepted()
{
    EntryPoint entryPoint(m_requestContext.data(), m_state, m_userId);

    bool result = (entryPoint.exec() == QDialog::Accepted);

    if (result)
    {
        m_state = entryPoint.currentState();
        m_userId = entryPoint.currentUserId();
        m_roleId = entryPoint.currentRoleId();
        m_permissionList = entryPoint.permissionList();
    }

    return result;
}

void Loader::start()
{
    loadModules();
    createWindow();
}

void Loader::stop()
{
//    m_requestContext->stop();

    destroyWindow();
    unloadModules();
}

void Loader::loadModules()
{
    QDir pluginsDir(QCoreApplication::applicationDirPath());

    if (pluginsDir.cd(SUBVIEWS_DIR))
    {
        foreach (const QString& fileName, pluginsDir.entryList(QStringList() << SHARED_LIBRARY_SUFFIX, QDir::Files))
        {
            QString moduleName = QFileInfo(fileName).baseName();

            QSharedPointer<QPluginLoader> loader(new QPluginLoader(pluginsDir.absoluteFilePath(fileName)));
            PluginInterface* const interface = qobject_cast<PluginInterface*>(loader->instance());

            if (interface)
            {
                if (!m_subviews.contains(interface->id()))
                {
                    SubviewWidget* subview = interface->createSubview();

                    if (subview->initialize(m_requestContext.data()))
                    {
                        m_loaders[moduleName] = loader;

                        m_subviews[interface->id()] = subview;
                        m_subviews[interface->id()]->setupUserPermissions(m_permissionList);
                    }
                    else
                    {
                        qWarning() << QString("Reject to load \"%1\": failed to initialize").arg(subview->name());
                        loader->unload();
                    }
                }
                else
                {
                    qWarning() << QString("Reject to load \"%1\": module id already exists").arg(interface->id());
                    loader->unload();

                    m_subviews[interface->id()]->setupUserPermissions(m_permissionList);
                }
            }
            else
                qWarning() << QString("Unable to load \"%1\": object is not a ModuleInterface instance").arg(fileName).toUtf8().data();
        }
    }
}

void Loader::unloadModules()
{
    foreach (const PluginLoaderPtr& loader, m_loaders.values())
        loader->unload();

    m_loaders.clear();
}

void Loader::createWindow()
{
    m_mainView.reset(new MainView(nullptr, m_subviews));

    connect(m_mainView.data(), &MainView::closeRequested, this, [this]
    {
        stop();

        if (authDialogAccepted())
            start();
        else
            qApp->exit();
    }, Qt::QueuedConnection);

    m_mainView->showMaximized();
}

void Loader::destroyWindow()
{
    m_mainView.reset();
    m_subviews.clear();
}
