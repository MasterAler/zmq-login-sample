#include "EntryPoint.h"
#include "ui_EntryPoint.h"

#include "Command.h"
#include "UserModel.h"
#include "RoleModel.h"

#include <QTimer>
#include <QDebug>

namespace
{
    const QString GET_USERS_COMMAND = "get_user_list";
    const QString GET_ROLES_COMMAND = "get_role_list";
    const QString GET_USER_ROLES_COMMAND = "get_pilot_role_list";
}

EntryPoint::EntryPoint(RequestContext* requestContext, State state, const QString& userId, QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::EntryPoint)
    , m_flashAuthFailTimer(new QTimer)
    , m_userModel(new UserModel(this))
    , m_requestContext(requestContext)
    , m_state(state)
    , m_userId(userId)
{
    ui->setupUi(this);

    ui->userLoginComboBox->setModel(m_userModel);

    m_submitDataHandler = [this](const QVariantMap& reply, const QVariant& label)
    {
        int errorCode = reply["error"].toInt();

        if (label == GET_USERS_COMMAND)
        {
            if (errorCode == Request::NoError)
            {
                m_userModel->fromList(reply["data"].toList());

                int currentIndex = m_userModel->indexById(m_userId).row();
                ui->userLoginComboBox->setCurrentIndex(currentIndex > 0 ? currentIndex : 0);
            }
            else if (errorCode == Request::OperationTimeout)
                Command::call(GET_USERS_COMMAND, QVariantMap(), m_requestContext, m_submitDataHandler, GET_USERS_COMMAND);
            else
                qCritical() << QString("%1 failed with %2").arg(GET_USERS_COMMAND).arg(errorCode);
        }
        else
        {
            switch (m_state)
            {
                case NotAuthenticated:
                {
                    if (errorCode == Request::NoError)
                    {
                        m_userId = reply["data"].toString();

                        if (!m_userId.isEmpty())
                        {
                            m_state = Authenticated;
                            Command::call(GET_ROLES_COMMAND, QVariantMap(), m_requestContext, m_submitDataHandler, GET_ROLES_COMMAND);
                        }
                        else
                            onAuthenticationFailed();
                    }
                }
                break;

                case Authenticated:
                {
//                    if (label == GET_ROLES_COMMAND)
//                    {
//                        if (errorCode == Request::NoError)
//                        {
//                            m_roleModel->fromList(reply["data"].toList());
//                            Command::call(GET_USER_ROLES_COMMAND, {{"user_id", m_userId}}, m_requestContext, m_submitDataHandler, GET_USER_ROLES_COMMAND);
//                        }
//                        else if (errorCode == Request::OperationTimeout)
//                            Command::call(GET_ROLES_COMMAND, QVariantMap(), m_requestContext, m_submitDataHandler, GET_ROLES_COMMAND);
//                        else
//                            qCritical() << QString("%1 failed with %2").arg(GET_ROLES_COMMAND).arg(errorCode);
//                    }
                }
                break;
            }
        }
    };

    void (QComboBox::*loginActivated)(int) = &QComboBox::activated;
    connect(ui->userLoginComboBox, loginActivated, this, &EntryPoint::dropAuthenticationState);

    connect(ui->acceptButton, &QPushButton::clicked, [this]()
    {
        QVariantMap args;

        switch (m_state)
        {
            case NotAuthenticated:
            {
                args["login"] = m_userModel->userInfo(ui->userLoginComboBox->currentIndex(), UserModel::Login);
                args["password"] = ui->passwordLineEdit->text();

                Command::call("authenticate_user", args, m_requestContext, m_submitDataHandler);
            }
            break;

            case Authenticated:
                doAccept();
            break;
        }
    });

    connect(ui->rejectButton, &QPushButton::clicked, [this]()
    {
        if (m_userModel->rowCount())
        {
            switch (m_state)
            {
                case NotAuthenticated:
                    reject();
                break;

                case Authenticated:
                    dropAuthenticationState();
                break;
            }
        }
        else
            reject();
    });

    Command::call(GET_USERS_COMMAND, QVariantMap(), m_requestContext, m_submitDataHandler, GET_USERS_COMMAND);

    if (Authenticated == m_state)
        Command::call(GET_ROLES_COMMAND, QVariantMap(), m_requestContext, m_submitDataHandler, GET_ROLES_COMMAND);
}

EntryPoint::~EntryPoint()
{}

QStringList EntryPoint::permissionList() const
{
    return m_permissionList;
}

EntryPoint::State EntryPoint::currentState() const
{
    return m_state;
}

QString EntryPoint::currentUserId() const
{
    return m_userId;
}

QString EntryPoint::currentRoleId() const
{
    return m_roleId;
}

void EntryPoint::dropAuthenticationState()
{
    m_state = NotAuthenticated;
    m_roleModel->clear();

    ui->passwordLineEdit->clear();
    ui->passwordLabel->show();
    ui->passwordLineEdit->show();
    ui->rejectButton->setText(tr("Выйти"));
}

void EntryPoint::onAuthenticationFailed()
{
    ui->passwordFrame->setStyleSheet("QFrame{ padding: 1px; background-color: red; }");

    ui->userLoginComboBox->setEnabled(false);
    ui->passwordLineEdit->setEnabled(false);

    connect(m_flashAuthFailTimer.data(), &QTimer::timeout, [this]()
    {
        ui->passwordFrame->setStyleSheet("QFrame{ padding: 1px; }");

        ui->userLoginComboBox->setEnabled(true);
        ui->passwordLineEdit->setEnabled(true);
    });

    m_flashAuthFailTimer->start(1000);
}

void EntryPoint::doAccept()
{
    // YOUR CHECKS HERE
    accept();
}
