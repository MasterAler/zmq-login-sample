#pragma once

#include <QDialog>
#include <QVariant>
#include <QVariant>

#include "Command.h"

class RequestContext;

namespace Ui
{
    class EntryPoint;
}

class UserModel;
class RoleModel;

class EntryPoint : public QDialog
{
    Q_OBJECT

public:
    enum State
    {
        NotAuthenticated,
        Authenticated
    };
    
public:
    explicit EntryPoint(RequestContext* requestContext,
                        State state,
                        const QString& userId,
                        QWidget* parent = nullptr);
    virtual ~EntryPoint();

public:
    QStringList permissionList() const;
    State currentState() const;
    QString currentUserId() const;
    QString currentRoleId() const;

private slots:
    void dropAuthenticationState();
    void onAuthenticationFailed();
    void doAccept();

private:
    QScopedPointer<Ui::EntryPoint> ui;
    QScopedPointer<QTimer> m_flashAuthFailTimer;
    UserModel* m_userModel;
    RoleModel* m_roleModel;
    RequestContext* m_requestContext;
    State m_state;

    QString m_userId;
    QString m_roleId;

    Command::HandlerType m_submitDataHandler;

    QStringList m_permissionList;
};

