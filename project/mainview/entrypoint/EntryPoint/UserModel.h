#pragma once

#include <QStandardItemModel>

class UserModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum Role
    {
        FirstName  = Qt::UserRole + 1,
        MiddleName = Qt::UserRole + 2,
        LastName   = Qt::UserRole + 3,
        UserId     = Qt::UserRole + 4,
        Login      = Qt::UserRole + 5
    };

public:
    explicit UserModel(QObject* parent = nullptr);
    virtual ~UserModel();

public:
    QVariant userInfo(int row, Role role) const;
    QVariant userInfo(const QModelIndex& index, Role role) const;

    QModelIndex indexById(const QString& id) const;

public slots:
    void fromList(const QVariantList& userList);
    void addUser(const QVariantMap& userObject);

public:
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
};
