#pragma once

#include <QDialog>

namespace Ui {
    class ModeSelectorDialog;
}

class ModeSelectorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ModeSelectorDialog(QWidget* parent = nullptr);
    virtual ~ModeSelectorDialog();

private:
    Ui::ModeSelectorDialog *ui;
};
