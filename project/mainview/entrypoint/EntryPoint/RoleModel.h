#pragma once

#include <QStandardItemModel>

class RoleModel : public QStandardItemModel
{
    Q_OBJECT

    enum Roles
    {
        IdRole = Qt::UserRole + 1,
        PermissionRole
    };

public:
    explicit RoleModel(QObject* parent = nullptr);

public:
    QStringList rolePermissions(const QModelIndex& index) const;

    QModelIndex indexById(const QString& id) const;
    QString idFromIndex(const QModelIndex& index) const;

public slots:
    void fromList(const QVariantList&data);
    void filterRoles(const QVariantList& roleIds);
};
