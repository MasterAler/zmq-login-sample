#include "RoleModel.h"


RoleModel::RoleModel(QObject* parent)
    : QStandardItemModel(parent)
{}

QStringList RoleModel::rolePermissions(const QModelIndex& index) const
{
    QStandardItem* item = itemFromIndex(index);
    return item ? item->data(PermissionRole).toString().split(';') : QStringList();
}

QString RoleModel::idFromIndex(const QModelIndex& index) const
{
    return index.data(IdRole).toString();
}

QModelIndex RoleModel::indexById(const QString& id) const
{
    QModelIndexList list = match(index(0 ,0), IdRole, id, 1, Qt::MatchFixedString);
    return !list.isEmpty() ? list.first() : QModelIndex();
}

void RoleModel::fromList(const QVariantList& data)
{
    clear();

    foreach(const QVariant& value, data)
    {
        QVariantMap roleData = value.toMap();

        QStandardItem* item = new QStandardItem(roleData["role"].toString());
        item->setData(roleData["id"], IdRole);
        item->setData(roleData["permissions"], PermissionRole);

        appendRow(item);
    }
}

void RoleModel::filterRoles(const QVariantList& roleIds)
{
    int k = 0;
    while (k < rowCount())
    {
        if (!roleIds.contains(index(k, 0).data(IdRole)))
            removeRow(k);
        else
            ++k;
    }
}
