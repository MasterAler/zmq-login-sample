#include "UserModel.h"

#include <QStandardItem>
#include <QVariantMap>

#include <QDebug>

UserModel::UserModel(QObject *parent)
    : QStandardItemModel(parent)
{}

UserModel::~UserModel()
{}

QVariant UserModel::userInfo(int row, Role role) const
{
    QStandardItem* modelItem = item(row);
    return modelItem ? modelItem->data(role) : QVariant();
}

QVariant UserModel::userInfo(const QModelIndex& index, Role role) const
{
    return userInfo(index.row(), role);
}

QModelIndex UserModel::indexById(const QString& id) const
{
    QModelIndexList list = match(index(0 ,0), UserId, id, 1, Qt::MatchFixedString);
    return !list.isEmpty() ? list.first() : QModelIndex();
}

void UserModel::fromList(const QVariantList& userList)
{
    Q_FOREACH (const QVariant& userItem, userList)
        addUser(userItem.toMap());
}

void UserModel::addUser(const QVariantMap& userObject)
{
    QStandardItem* userItem = new QStandardItem;

    userItem->setData(userObject["first_name"], FirstName);
    userItem->setData(userObject["middle_name"], MiddleName);
    userItem->setData(userObject["last_name"], LastName);
    userItem->setData(userObject["login"], Login);
    userItem->setData(userObject["user_id"], UserId);

    appendRow(userItem);
}

QVariant UserModel::data(const QModelIndex& index, int role) const
{
    QVariant result = QStandardItemModel::data(index, role);

    if ((role == Qt::DisplayRole) || (role == Qt::EditRole))
    {
        QStandardItem* userItem = itemFromIndex(index);

        result = QStringLiteral("%1 %2. %3.").arg(userItem->data(LastName).toString(),
                                                  userItem->data(FirstName).toString().left(1),
                                                  userItem->data(MiddleName).toString().left(1));
    }

    return result;
}
