#include "ModeSelectorDialog.h"
#include "ui_ModeSelectorDialog.h"

ModeSelectorDialog::ModeSelectorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModeSelectorDialog)
{
    ui->setupUi(this);

    connect(ui->mode1Button, &QAbstractButton::clicked, this, &QDialog::accept);
    connect(ui->mode2Button, &QAbstractButton::clicked, this, &QDialog::accept);
    connect(ui->mode3Button, &QAbstractButton::clicked, this, &QDialog::accept);
}

ModeSelectorDialog::~ModeSelectorDialog()
{
    delete ui;
}
