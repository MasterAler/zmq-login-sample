#pragma once

#include <QObject>
#include <QScopedPointer>
#include <QHash>

#include "RequestContext.h"
#include "EntryPoint.h"

class QPluginLoader;
class MainView;
class SubviewWidget;
class EntryPoint;

typedef QSharedPointer<QPluginLoader> PluginLoaderPtr;

class Loader : public QObject
{
    Q_OBJECT

public:
    explicit Loader(QObject* parent = nullptr);
    virtual ~Loader();

public:
    bool authDialogAccepted();

public slots:
    void start();
    void stop();

    void loadModules();
    void unloadModules();

    void createWindow();
    void destroyWindow();

private:
    QHash<QString, PluginLoaderPtr> m_loaders;
    QHash<QString, SubviewWidget*> m_subviews;

    QScopedPointer<RequestContext> m_requestContext;
    QScopedPointer<MainView> m_mainView;

    EntryPoint::State m_state;

    QString m_userId;
    QString m_roleId;
    QStringList m_permissionList;
};

