SOURCES = \
    ../subview/Subview.cpp \
    ../interfaces/SubviewWidget.cpp \
    ../command/Command.cpp \
    ../command/Request.cpp \
    ../command/RequestContext.cpp \
    $$PWD/main.cpp \
    $$PWD/MainView.cpp \
    $$PWD/Loader.cpp \
    $$PWD/EntryPoint/EntryPoint.cpp \
    $$PWD/EntryPoint/RoleModel.cpp \
    $$PWD/EntryPoint/UserModel.cpp \
    $$PWD/EntryPoint/ModeSelectorDialog.cpp \

HEADERS = \
    ../subview/Subview.h \
    ../interfaces/SubviewWidget.h \
    ../command/Command.h \
    ../command/Request.h \
    ../command/RequestContext.h \
    $$PWD/MainView.h \
    $$PWD/Loader.h \
    $$PWD/EntryPoint/EntryPoint.h \
    $$PWD/EntryPoint/RoleModel.h \
    $$PWD/EntryPoint/UserModel.h \
    $$PLATFORM/zmq/include/zmq.h \
    $$PLATFORM/zmq/include/zmq.hpp \
    $$PLATFORM/zmq/include/zmq_utils.h \
    $$PLATFORM/zmq/include/nzmqt/global.hpp \
    $$PLATFORM/zmq/include/nzmqt/impl.hpp \
    $$PLATFORM/zmq/include/nzmqt/nzmqt.hpp \
    $$PWD/EntryPoint/ModeSelectorDialog.h \

FORMS   = \
    $$PWD/MainView.ui \
    $$PWD/EntryPoint/EntryPoint.ui \
    $$PWD/EntryPoint/ModeSelectorDialog.ui

RESOURCES += \
    Resources.qrc
