#include "MainView.h"
#include "ui_MainView.h"

#include "SubviewWidget.h"

#include <QCloseEvent>
#include <QPushButton>

#include <QDebug>

MainView::MainView(QWidget* parent, const SubviewDict& subviewDict)
    : QWidget(parent)
    , ui(new Ui::MainView)
    , m_exitButton(new QPushButton(tr("Выход")))
    , m_subviews(subviewDict)
{
    ui->setupUi(this);

    connect(m_exitButton, &QPushButton::clicked, this, &QWidget::close);
    ui->moduleTabWidget->setCornerWidget(m_exitButton);

    connect(ui->moduleTabWidget, &QTabWidget::currentChanged, [this](int tabIndex)
    {
        SubviewWidget* subview = static_cast<SubviewWidget*>(ui->moduleTabWidget->widget(tabIndex));

        if (subview)
            subview->activate();
        else
            qDebug() << QString("Wrong tab index: %1").arg(tabIndex);
    });

    setupSubviews(m_subviews);
}

MainView::~MainView()
{}

void MainView::setupSubviews(const SubviewDict& subviewDict)
{
    m_subviews = subviewDict;

    if (!m_subviews.empty())
        ui->moduleTabWidget->clear();

    for (SubviewDict::ConstIterator i = m_subviews.constBegin(), e = m_subviews.constEnd(); i != e; ++i)
        ui->moduleTabWidget->addTab(i.value(), i.value()->name());
}

void MainView::closeEvent(QCloseEvent* event)
{
    foreach (SubviewWidget* subview, m_subviews.values())
        subview->stop();

    emit closeRequested();

    event->ignore();
}

