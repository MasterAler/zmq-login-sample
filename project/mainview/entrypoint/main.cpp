#include "Loader.h"

#include <QApplication>
#include <QDir>

#include "DailyFileAppender.h"
#include "ConsoleAppender.h"
#include "Logger.h"

namespace
{
    const QString LOG_DIR = QStringLiteral("logs");

    void setupLogger()
    {
        auto* const appender = new ConsoleAppender;
        // auto* const appender = new DailyFileAppender(QStringLiteral("%1/%2").arg(QCoreApplication::applicationDirPath(), LOG_DIR));
        appender->setFormat(QStringLiteral("[%t] %-7L %-40c %4i %m\n"));
        Logger::registerAppender(appender);
    }
}

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

#ifdef Q_OS_LINUX
    // NOTE: Needed because of the way AstraLinux's fly-fm file manager launches executables.
    QDir::setCurrent(QCoreApplication::applicationDirPath());
#endif
    setupLogger();

    LOG_INFO(QStringLiteral("-- Application started v.%1 --").arg("APP_VERSION"));

    Loader loader;

    int result = EXIT_SUCCESS;

    if (loader.authDialogAccepted())
    {
        loader.start();
        result = app.exec();
    }

    LOG_INFO(QStringLiteral("-- Application closed --"));

    return result;
}
