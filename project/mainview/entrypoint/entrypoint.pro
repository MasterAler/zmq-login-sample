TEMPLATE = app
QT += core widgets network

isEmpty(PLATFORM): PLATFORM = $$PWD/../../../platform

CONFIG += c++11

isEmpty(BIN_DIR) {
    BIN_DIR = $$PWD/../../../bin
    CONFIG(debug, debug|release):BIN_DIR = $$join(BIN_DIR,,,_debug)
}
DESTDIR=$$BIN_DIR

LOGGER_LIB = logger
CONFIG(debug, debug|release):LOGGER_LIB = $$join(LOGGER_LIB,,,d)

CONFIG(release, debug|release):ZMQ_LIB = libzmq-v110-mt-4_0_4
CONFIG(debug, debug|release):ZMQ_LIB = libzmq-v110-mt-gd-3_2_4

INCLUDEPATH *= \
    ../subview \
    ../interfaces \
    ../command \
    ../permissions/permission_loader \
    $$PWD/EntryPoint \
    $$PLATFORM/cutelogger/include \
    $$PLATFORM/zmq/include \

DEPENDPATH *= \
    $$INCLUDEPATH

LIBS *= \
    -L"$$DESTDIR" \
    -L"$$PLATFORM/zmq/lib/win32" \
    -l$$LOGGER_LIB \
    -l$$ZMQ_LIB

# constant, enabling/disabling authorized access, for developers convenience
DEFINES += AUTH

include(sources.pri)
