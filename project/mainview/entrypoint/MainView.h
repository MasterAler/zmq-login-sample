#pragma once

#include <QWidget>

#include <QHash>
#include <QScopedPointer>

namespace Ui
{
    class MainView;
}

class SubviewWidget;
class QPushButton;
class QCloseEvent;

typedef QHash<QString, SubviewWidget*> SubviewDict;

class MainView : public QWidget
{
    Q_OBJECT

public:
    explicit MainView(QWidget* parent = nullptr, const SubviewDict& subviewDict = SubviewDict());
    virtual ~MainView();

public slots:
    void setupSubviews(const SubviewDict& subviewDict);

public:
    void closeEvent(QCloseEvent* event) override;

signals:
    void closeRequested();

private:
    QScopedPointer<Ui::MainView> ui;
    QPushButton* m_exitButton;

    SubviewDict m_subviews;
};
