TEMPLATE = subdirs

BIN_DIR = ../../bin
CONFIG(debug, debug|release):BIN_DIR = $$join(BIN_DIR,,,_debug)

SUBDIRS = \
    entrypoint

SUBVIEW_DIR = $${BIN_DIR}/subview
SUBVIEW_DIR ~= s,/,\\,g

SOURCE_DIR = $$PWD
SOURCE_DIR ~= s,/,\\,g

for(DIR, SUBDIRS) {
    SOURCE_FILENAME = $${SOURCE_DIR}\\$${DIR}\\$${DIR}.permissions
    PERMISSIONS_FILENAME = $${DIR}
    CONFIG(debug, debug|release):PERMISSIONS_FILENAME = $$join(PERMISSIONS_FILENAME,,,d)
    DEST_FILENAME = $${SUBVIEW_DIR}\\$${PERMISSIONS_FILENAME}.permissions

    exists($$DEST_FILENAME)
    {
        system(del $$DEST_FILENAME)
    }
    system(copy $$SOURCE_FILENAME $$DEST_FILENAME)
}
