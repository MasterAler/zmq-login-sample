#include "Request.h"

#include "nzmqt/nzmqt.hpp"

#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QFile>

#include <QDebug>

namespace
{   
    QString readConfig()
    {
        QFile config("request.config");
        return config.open(QFile::ReadOnly) ? config.readAll().trimmed() : QByteArray("tcp://127.0.0.1:43000");
    }

    const QString HOST = readConfig();

    bool isSchemaValid(const QVariantMap& data)
    {
        return data.contains("data")  &&
               data.contains("error") && data["error"].canConvert(QVariant::Int);
    }
}

class RequestPrivate
{
public:
    explicit RequestPrivate(QObject* parent, nzmqt::ZMQContext* zmqContext)
        : m_timeoutTimer(new QTimer(parent))
        , m_ioSocket(zmqContext->createSocket(nzmqt::ZMQSocket::TYP_REQ, parent))
    {
        m_timeoutTimer->setSingleShot(true);
        m_ioSocket->setLinger(0);

        try
        {
            m_ioSocket->connectTo(HOST);
        }
        catch (...)
        {
            qWarning() << "failed to connect: " << HOST;
        }
    }

public:
    QTimer* m_timeoutTimer;
    nzmqt::ZMQSocket* m_ioSocket;
};

QString Request::errorString(int error)
{
    QString result;

    switch (error)
    {
        case BadServerReply:
            result = tr("Неверный ответ сервера");
        break;

        case OperationTimeout:
            result = tr("Время ожидания операции истекло");
        break;
    }

    return result;
}

Request::Request(RequestContext* requestContext, QObject* parent)
    : QObject(parent)
    , d(new RequestPrivate(this, requestContext ? requestContext->m_context : RequestContext::defaultContext()))
{
    connect(d->m_timeoutTimer, &QTimer::timeout, [this]
    {
        d->m_ioSocket->close();

        QVariantMap result;
        result["error"] = OperationTimeout;

        emit finished(result);
    });

    connect(d->m_ioSocket, &nzmqt::ZMQSocket::messageReceived, [this](const QByteArray& data)
    {
        d->m_timeoutTimer->stop();

        QJsonParseError parseError;
        QVariantMap result = QJsonDocument::fromJson(data, &parseError).object().toVariantMap();

        if (parseError.error != QJsonParseError::NoError)
        {
            qCritical() << parseError.errorString();
            result["error"] = BadServerReply;
        }
        else if (!isSchemaValid(result))
        {
            qCritical() << QString("invalid schema");
            result["error"] = BadServerReply;
        }

        emit finished(result);
    });
}

Request::~Request()
{}

void Request::send(const QByteArray& data, int timeout_ms)
{
    try
    {
        d->m_ioSocket->sendMessage(data);
    }
    catch(...)
    {
        qWarning() << "failed to send: " << HOST;
    }

    d->m_timeoutTimer->start(timeout_ms);
}
