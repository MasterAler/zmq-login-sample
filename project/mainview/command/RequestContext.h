#pragma once

namespace nzmqt
{
    class ZMQContext;
}

class RequestContext
{
public:
    RequestContext();
    ~RequestContext();

friend class Request;

public:
    void stop();

private:
    static nzmqt::ZMQContext* defaultContext();
    nzmqt::ZMQContext* m_context;
};

