#include "Command.h"

#include <QJsonObject>
#include <QJsonValue>
#include <QJsonDocument>
#include <QHostInfo>

static QString LOCAL_HOST_NAME = QHostInfo::localHostName();

QByteArray Command::prepareCommand(const QString& command, const QVariantMap& args)
{
    QVariantMap result;

    result["command"]   = command;
    result["args"]      = QJsonObject::fromVariantMap(args);
    result["sender"]    = LOCAL_HOST_NAME;

    return QJsonDocument(QJsonObject::fromVariantMap(result)).toJson(QJsonDocument::Compact);

//    return QJsonDocument(QJsonObject(
//    {
//        {"command", command},
//        {"args"   , QJsonObject::fromVariantMap(args)},
//        {"sender" , LOCAL_HOST_NAME}
//    })).toJson(QJsonDocument::Compact);
}

void Command::call(const QString& command, const QVariantMap& args, RequestContext* requestContext, HandlerType handler, const QVariant& label, int timeout_ms)
{
    Request* request = new Request(requestContext);

    QObject::connect(request, &Request::finished, [handler, label, request](const QVariantMap& data)
    {
        handler(data, label);
        request->deleteLater();
    });

    request->send(prepareCommand(command, args), timeout_ms);
}
