#pragma once

#include <QObject>
#include "RequestContext.h"

class RequestPrivate;

class Request : public QObject
{
    Q_OBJECT

public:
    enum Error
    {
        NoError = 0,
        BadServerReply,
        OperationTimeout
    };

    static QString errorString(int error);

public:
    explicit Request(RequestContext* requestContext = nullptr, QObject* parent = nullptr);
    virtual ~Request();

public slots:
    void send(const QByteArray& data, int timeout_ms = 5000);

signals:
    void finished(const QVariantMap&);

private:
    QScopedPointer<RequestPrivate> d;
};
