#pragma once

#include "Request.h"
#include <QVariantMap>
#include <functional>

namespace Command
{
    typedef std::function<void(const QVariantMap&, const QVariant)> HandlerType;

    QByteArray prepareCommand(const QString& command, const QVariantMap& args);

    void call(const QString& command,
              const QVariantMap& args,
              RequestContext* requestContext,
              HandlerType handler,
              const QVariant& label = QVariant(),
              int timeout_ms = 5000);
}
