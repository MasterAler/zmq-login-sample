#include "RequestContext.h"

#include "nzmqt/nzmqt.hpp"
#include <QCoreApplication>

RequestContext::RequestContext()
    : m_context(RequestContext::defaultContext())
{}

RequestContext::~RequestContext()
{
    qApp->disconnect(m_context);
}

void RequestContext::stop()
{
    m_context->stop();
}

nzmqt::ZMQContext* RequestContext::defaultContext()
{
    static nzmqt::ZMQContext* context = nzmqt::createDefaultContext();

    if (context->isStopped())
        context->start();

    return context;
}
