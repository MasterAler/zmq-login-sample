SOURCES += \
    $$PWD/command/Command.cpp \
    $$PWD/command/Request.cpp \
    $$PWD/command/RequestContext.cpp

HEADERS += \
    $$PWD/command/Command.h \
    $$PWD/command/Request.h \
    $$PWD/command/RequestContext.h \
    $$PLATFORM/zmq/include/zmq.h \
    $$PLATFORM/zmq/include/zmq.hpp \
    $$PLATFORM/zmq/include/zmq_utils.h \
    $$PLATFORM/zmq/include/nzmqt/global.hpp \
    $$PLATFORM/zmq/include/nzmqt/impl.hpp \
    $$PLATFORM/zmq/include/nzmqt/nzmqt.hpp
