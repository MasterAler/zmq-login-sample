TEMPLATE = lib

DEFINES += LOCAL_SERVER

isEmpty(PLATFORM): PLATFORM = $$PWD/../../platform

isEmpty(BIN_DIR) {
    BIN_DIR = $$PWD/../../bin
    CONFIG(debug, debug|release):BIN_DIR = $$join(BIN_DIR,,,_debug)
}
DESTDIR = $$join(BIN_DIR,,,/subview)

QT += widgets network
CONFIG += plugin c++11

CONFIG(debug, debug|release):TARGET = $$join(TARGET,,,d)

# attach ZMQ library (with its binaries)

CONFIG(release, debug|release):ZMQ_LIB = libzmq-v110-mt-4_0_4
CONFIG(debug, debug|release):ZMQ_LIB = libzmq-v110-mt-gd-3_2_4

win32 {
    DESTDIR_WIN = $${DESTDIR}
    DESTDIR_WIN ~= s,/,\\,g
    LIBDIR_WIN = $${PLATFORM}
    LIBDIR_WIN ~= s,/,\\,g

    !exists($${DESTDIR_WIN}\libzmq-v110*.dll)
    {
        CONFIG(release, debug|release): system( copy $${LIBDIR_WIN}\zmq\bin\libzmq-v110-mt-3_2_4.dll $${DESTDIR_WIN}\..\libzmq-v110-mt-3_2_4.dll)
        CONFIG(debug, debug|release): system( copy $${LIBDIR_WIN}\zmq\bin\libzmq-v110-mt-gd-3_2_4.dll $${DESTDIR_WIN}\..\libzmq-v110-mt-gd-3_2_4.dll)
    }
}

LOGGER_LIB = logger
CONFIG(debug, debug|release):LOGGER_LIB = $$join(LOGGER_LIB,,,d)

INCLUDEPATH *= \
    $$PWD/interfaces \
    $$PWD/subview \
    $$PWD/command \
    $$PLATFORM/zmq/include \
    $$PLATFORM/cutelogger/include

LIBS *= \
    -L"$$PLATFORM/zmq/lib/win32" \
    -L"$$BIN_DIR" \
    -l$$ZMQ_LIB \
    -l$$LOGGER_LIB

DEPENDPATH += \
    $$INCLUDEPATH

include(pluginSources.pri)
