#pragma once

#include "ModuleInterface.h"

#include <QVariantMap>
#include <QWidget>

class RequestContext;

class Subview : public QWidget, public ModuleInterface
{
    Q_OBJECT
    Q_INTERFACES(ModuleInterface)

public:
    explicit Subview(QWidget* parent = nullptr);
    virtual ~Subview();

public:
    QByteArray id() const override;
    QString name() const override;
    QWidget* widget() const override;

public slots:
    bool initialize(RequestContext* requestContext);

    void activate() override;
    void stop() override;

public:
    virtual QVariantMap modulePermissions() const;
    virtual void setupUserPermissions(const QStringList&);

protected:
    virtual bool initializeImpl();

protected:
    RequestContext* m_requestContext; // doesn't own. For access only
    bool m_hasActiveRequest;
    bool m_stopRequested;
};
