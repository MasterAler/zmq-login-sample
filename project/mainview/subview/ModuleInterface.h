#pragma once

#include <QtPlugin>

class QWidget;

class ModuleInterface
{
public:
    virtual ~ModuleInterface() {}

    virtual void activate() = 0;
    virtual void stop() = 0;

    virtual QByteArray id() const = 0;
    virtual QString name() const = 0;
    virtual QWidget* widget() const = 0;
};

#define ModuleInterface_iid "org.geeksoft.qt.ModuleInterface"

Q_DECLARE_INTERFACE(ModuleInterface, ModuleInterface_iid)
