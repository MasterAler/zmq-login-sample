TEMPLATE = subdirs

!isEmpty(PLATFORM): message("carburator.pro: External PLATFORM parameter given: $$PLATFORM")
isEmpty(PLATFORM): PLATFORM = $$PWD/../platform

!isEmpty(BIN_DIR): message("carburator.pro: External BIN_DIR parameter given: $$BIN_DIR")
isEmpty(BIN_DIR) {
    BIN_DIR = $$PWD/../bin
    CONFIG(debug, debug|release):BIN_DIR = $$join(BIN_DIR,,,_debug)
}
DESTDIR=$$BIN_DIR

SUBDIRS = \
    cutelogger \
    mainview

mainview.depends = cutelogger

!linux:!win32:error(Unsupported platform)
