#pragma once

#include "FileAppender.h"
#include <QtCore/QDir>


class DailyFileAppender : public FileAppender
{
public:
    explicit DailyFileAppender(const QString& dirName = QString());

    QString dirName() const;
    void setDirName(const QString&);

protected:
    virtual void append(const QDateTime& timeStamp, Logger::LogLevel logLevel, const char* file, int line,
                        const char* function, const QString& message);

protected:
    void checkCurrentDay();
    void removeIfExpired(const int day);
    QString dayToFilename(const int day) const;

private:
    QDir m_logDir;
    int m_day;
    mutable QMutex m_logDirMutex;
};
