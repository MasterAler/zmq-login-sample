#include "DailyFileAppender.h"
#include <QDateTime>

namespace
{
    const QString SUFFIX = QStringLiteral("log");
}

DailyFileAppender::DailyFileAppender(const QString& dirName)
{
    setDirName(dirName);
}

void DailyFileAppender::checkCurrentDay()
{
    const int day = QDateTime::currentDateTime().date().day();

    if (day != m_day)
    {
        m_day = day;
        removeIfExpired(m_day);
        setFileName(QStringLiteral("%1/%2").arg(m_logDir.dirName(), dayToFilename(m_day)));
    }
}

void DailyFileAppender::removeIfExpired(const int day)
{
    const QString dayFilePath = QStringLiteral("%1/%2").arg(m_logDir.canonicalPath(), dayToFilename(day));
    const int month = QDateTime::currentDateTime().date().month();

    if (month != QFileInfo(dayFilePath).lastModified().date().month())
        QFile::remove(dayFilePath);
}

QString DailyFileAppender::dayToFilename(const int day) const
{
    return QStringLiteral("%1.%2").arg(day, 2, 10, QChar('0')).arg(SUFFIX);
}

void DailyFileAppender::append(const QDateTime& timeStamp, Logger::LogLevel logLevel, const char* file, int line,
                               const char* function, const QString& message)
{
    checkCurrentDay();
    FileAppender::append(timeStamp, logLevel, file, line, function, message);
}

QString DailyFileAppender::dirName() const
{
    QMutexLocker locker(&m_logDirMutex);
    return m_logDir.dirName();
}

void DailyFileAppender::setDirName(const QString& dirName)
{
    QMutexLocker locker(&m_logDirMutex);

    if (dirName != m_logDir.dirName())
    {
        if (!m_logDir.mkpath(dirName))
            qCritical() << QStringLiteral("Can't create directory \"%1\"").arg(m_logDir.dirName());

        m_logDir.setPath(dirName);
        checkCurrentDay();
    }
}




