# from studymanager import studyapi
from usermanager import userapi
# from pilotmanager import pilotapi
# from configmanager import configapi
# from plantable import plantableapi
# from pilotpagecontroller import pilotpagecontrollerapi

handlers = {}

def append(new_handlers):
    for (k, v) in new_handlers.items():
        if k not in handlers:
            handlers[k] = v
        else:
            print ("DUPLICATE HANDLER: %s") % k
            
# append(studyapi.handlers)
append(userapi.handlers)
# append(pilotapi.handlers)
# append(configapi.handlers)
# append(plantableapi.handlers)
# append(pilotpagecontrollerapi.handlers)