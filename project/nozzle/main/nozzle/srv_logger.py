# -*- coding: utf-8 -*-

import logging
import logging.handlers

LOG_FILENAME = "./nozzle_access.log"
ERROR_LOG_FILENAME  = "./nozzle_error.log"
LOG_LEVEL = logging.DEBUG

# Make a class we can use to capture stdout and sterr in the log
class SrvLogger(object):
    def __init__(self, logger, level):
        """Needs a logger and a logger level."""
        self.logger = logging.getLogger(__name__)
        self.level = level
        self.logger.setLevel(level)
        
        # Make a handler that writes to a file, making a new file at midnight and keeping 3 backups
        handler = logging.handlers.TimedRotatingFileHandler(logger, when="midnight")
        formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
        handler.setFormatter(formatter)
        
        self.logger.addHandler(handler)
        
    def get_logger(self):
        return self.logger

    def write(self, message):
        # Only log if there is a message (not just a new line)
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())

