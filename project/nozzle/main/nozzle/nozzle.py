# -*- coding: utf-8 -*-

import sys
import signal
import getopt
import json
import zmq
from time import sleep
from threading import Thread
from srv_logger import *
from fuel import handlers

##### DEFAULT OPTIONS ###########   

BIND_ADDRESS = 'tcp://127.0.0.1:43000'

##################################   

### INIT LOGGER SECTION #########   
srv = SrvLogger(LOG_FILENAME, LOG_LEVEL)
# err_srv = SrvLogger(ERROR_LOG_FILENAME, logging.ERROR)
logger = srv.get_logger()

def log_info(msg):
    print(msg)
    logger.info(msg)

def log_error(msg):
    print(msg)
    logger.error(msg)
#################################   

### Help page 
def usage():
    print("The usage:")
    print("    -h, --help    Show this help")
    print("    -a, --addr 'your_connection_string'  listen given address.")
    print("                Addr example: 'tcp://192.168.10.201:43001' or 'tcp://eth0:43001'")

### To finish all matters gracefully and exit
def graceful_shutdown(code):
    log_arc_timer.cancel()
    sys.exit(code)

### ARGUMENTS PARSING SECTION ###   
try:
    opts, args = getopt.getopt(sys.argv[1:], "ha:",["help","addr="])
except getopt.GetoptError as err:
    print(str(err))
    usage()
    graceful_shutdown(2)
 
for opt, a in opts:
    if opt in ('-h', '--help'):
        usage()
        graceful_shutdown(0)
    elif opt in ('-a', '--addr'):
        BIND_ADDRESS=a
#################################   

# Makes the graceful stop of the server
# (must be called only from the base controlling thread)
def graceful_stop(code):

    global srv_thread
    global exit_request_flag

    log_info("Shutting down...")

    if (srv_thread): 
        exit_request_flag = True
        log_info("Waiting for server thread to finish...")
        srv_thread.join();

    log_info("Bye!")
    sys.exit(code) 

# Catches the system signals to (to shutdown) NOTE: in the main thread only
def break_handler(*args):
    print("Got break signal.")
    graceful_stop(0)

term_signals=[signal.SIGABRT, signal.SIGILL, signal.SIGINT, signal.SIGSEGV, signal.SIGTERM]
if hasattr(signal, 'SIGBREAK'):
    term_signals.append(signal.SIGBREAK)

for sig in term_signals:
    signal.signal(sig, break_handler)

# server message-processing thread
def server_thread():

    global exit_request_flag

    context = zmq.Context()
    socket = context.socket(zmq.REP)
    poller = zmq.Poller()

    try:
        socket.bind(BIND_ADDRESS)
    except zmq.error.ZMQError as err:
        print("Can't bind to the '" + BIND_ADDRESS + "' address, have to stop server.")
        return

    log_info("Listening: " + BIND_ADDRESS)
    print("You may press Ctrl-C (Ctrl-Break) to stop the server gracefully.")
    
    poller.register(socket, zmq.POLLIN)

    while True:

        # graceful message-processing termination
        if exit_request_flag:
            poller.unregister(socket)
            socket.close();            
            return 0;

        socks = dict(poller.poll(1000))

        if not socket in socks:
            continue

        data = socket.recv().decode('utf8')
        try:
            print(data.encode("utf8"))  # fixes win fixed byte encoding
            logger.info(data.encode("utf8"))

            commandlet = json.loads(data)
            command = commandlet["command"]
            if command in handlers:
                socket.send_string(handlers[command](**commandlet['args']))
            else:
                logger.error('bad fuel: %s' % command)
                print(b'bad fuel')
                socket.send(b'["bad fuel"]')
        except Exception as e:
            log_error(e)
            socket.send(b'Grats. You broke it.')
    

# main (controlling) thread
def run():

    # Is set to True when we need to stop the server 
    # sets only by controlling (this) thread
    global exit_request_flag
    # The server message-processing thread object
    global srv_thread

    exit_request_flag = False

    log_info("Starting Carburator Nozzle server")    

    # starting main server thread
    log_info("starting server thread");
    srv_thread = Thread(target = server_thread)
    srv_thread.start();
  
    # sleeping technical thread (potentially for managing operations) 
    # NOTE: don't remove it - or it will be blocked till the server thread 
    # stops and thus will not be able to react on external signals
    while True:
        if not srv_thread.is_alive():
            graceful_stop(1)
        sleep(1) 



if __name__ == '__main__':
    # Replace stdout with logging to file at INFO level
    # sys.stdout = srv
    # Replace stderr with logging to file at ERROR level
    # sys.stderr = err_srv
    run()
