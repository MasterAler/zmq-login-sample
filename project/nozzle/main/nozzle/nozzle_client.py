import zmq
import json
import getopt
import sys

CONN_ADDR='tcp://carbsrv.su27:43000' 

def usage():
    print("The usage:")
    print("    -h, --help    Show this help")
    print("    -a, --addr 'your_connection_string'  connect to given address. Addr example: 'tcp://carbsrv.su27:43001' ")
    

class Client:
    def __init__(self, addr):
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect(addr)

    def get(self):
        self.socket.send(json.dumps({
            'command' : 'load_study_program', 
            'args' : {}
        }).encode('utf8'))
        
        return self.socket.recv()

    def set(self, data):
        self.socket.send(json.dumps({
                'command' : 'save_study_program', 
                'args' : {'data' : data}
            }).encode('utf8'))
        
        return self.socket.recv()


print("Small carburator client")

### ARGUMENTS PARSING SECTION ###   
try:
    opts, args = getopt.getopt(sys.argv[1:], "ha:",["help","addr="])
except getopt.GetoptError as err:
    print(str(err))
    usage()
    sys.exit(2)
 
for opt, a in opts:
    if opt in ('-h', '--help'):
        usage()
        sys.exit()
    elif opt in ('-a', '--addr'):
        CONN_ADDR=a
#################################   

print("Using the connection: " + CONN_ADDR)

c = Client(CONN_ADDR)

print (c.get())
print (c.set("abcd"))
print (c.get())

print("Done")
