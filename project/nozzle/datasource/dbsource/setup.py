try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'UCC database manager',
    'author': 'aler',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'My email.',
    'version': '0.0.5',
    'install_requires': ['nose'],
    'packages': ['dbsource'],
    'scripts': [],
    'name': 'dbsource'
}

setup(**config)