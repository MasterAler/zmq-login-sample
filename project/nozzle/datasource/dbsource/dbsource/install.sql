--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.6
-- Dumped by pg_dump version 9.2.6
-- Started on 2018-09-19 18:24:42

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2049 (class 1262 OID 12002)
-- Dependencies: 2048
-- Name: postgres; Type: COMMENT; Schema: -; Owner: root
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- TOC entry 184 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 184
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 527 (class 1247 OID 33411)
-- Name: officials_type; Type: TYPE; Schema: public; Owner: ksvd42
--

CREATE TYPE officials_type AS (
	id integer,
	last_name text,
	first_name text,
	middle_name text,
	rank_id integer,
	misc text
);


ALTER TYPE public.officials_type OWNER TO ksvd42;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 169 (class 1259 OID 33495)
-- Name: pilot_classes; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE pilot_classes (
    id integer NOT NULL,
    pilot_class character varying(30),
    pos smallint NOT NULL
);


ALTER TABLE public.pilot_classes OWNER TO ksvd42;

--
-- TOC entry 170 (class 1259 OID 33498)
-- Name: pilot_classes_order_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE pilot_classes_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pilot_classes_order_seq OWNER TO ksvd42;

--
-- TOC entry 2052 (class 0 OID 0)
-- Dependencies: 170
-- Name: pilot_classes_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE pilot_classes_order_seq OWNED BY pilot_classes.pos;


--
-- TOC entry 171 (class 1259 OID 33514)
-- Name: pilot_roles; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE pilot_roles (
    id integer NOT NULL,
    pilot_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.pilot_roles OWNER TO ksvd42;

--
-- TOC entry 172 (class 1259 OID 33517)
-- Name: pilot_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE pilot_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pilot_roles_id_seq OWNER TO ksvd42;

--
-- TOC entry 2053 (class 0 OID 0)
-- Dependencies: 172
-- Name: pilot_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE pilot_roles_id_seq OWNED BY pilot_roles.id;


--
-- TOC entry 173 (class 1259 OID 33522)
-- Name: users; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE users (
    user_id integer NOT NULL,
    last_name character varying(50) DEFAULT ''::character varying NOT NULL,
    first_name character varying(50) DEFAULT ''::character varying NOT NULL,
    middle_name character varying(50) DEFAULT ''::character varying NOT NULL,
    login character varying(50) NOT NULL,
    password character varying(56) DEFAULT 'a7470858e79c282bc2f6adfd831b132672dfd1224c1e78cbf5bcd057'::character varying NOT NULL,
    rank_id integer DEFAULT (-1),
    rating real DEFAULT 1 NOT NULL,
    misc_info text DEFAULT ''::text
);


ALTER TABLE public.users OWNER TO ksvd42;

--
-- TOC entry 174 (class 1259 OID 33535)
-- Name: pilots; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE pilots (
    callsign character varying(5) DEFAULT ''::character varying NOT NULL,
    pilot_class_id integer DEFAULT (-1) NOT NULL,
    meteomin_day integer DEFAULT (-1) NOT NULL,
    meteomin_night integer DEFAULT (-1) NOT NULL,
    last_flight_day date,
    last_flight_night date,
    familiar_planes character varying(75) DEFAULT ''::character varying NOT NULL,
    division character varying(25) DEFAULT ''::character varying NOT NULL
)
INHERITS (users);


ALTER TABLE public.pilots OWNER TO ksvd42;

--
-- TOC entry 175 (class 1259 OID 33595)
-- Name: positions; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE positions (
    id integer NOT NULL,
    position_title text DEFAULT ''::text NOT NULL,
    position_group_id integer,
    position_code text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.positions OWNER TO ksvd42;

--
-- TOC entry 176 (class 1259 OID 33603)
-- Name: positions_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.positions_id_seq OWNER TO ksvd42;

--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 176
-- Name: positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE positions_id_seq OWNED BY positions.id;


--
-- TOC entry 177 (class 1259 OID 33605)
-- Name: ranks; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE ranks (
    id smallint NOT NULL,
    full_rank character varying(50) NOT NULL,
    short_rank character varying(20) DEFAULT 'тов.'::character varying NOT NULL,
    pos smallint NOT NULL
);


ALTER TABLE public.ranks OWNER TO ksvd42;

--
-- TOC entry 178 (class 1259 OID 33609)
-- Name: ranks_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE ranks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ranks_id_seq OWNER TO ksvd42;

--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 178
-- Name: ranks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE ranks_id_seq OWNED BY ranks.id;


--
-- TOC entry 179 (class 1259 OID 33611)
-- Name: ranks_order_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE ranks_order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ranks_order_seq OWNER TO ksvd42;

--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 179
-- Name: ranks_order_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE ranks_order_seq OWNED BY ranks.pos;


--
-- TOC entry 180 (class 1259 OID 33613)
-- Name: roles; Type: TABLE; Schema: public; Owner: ksvd42; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    role character varying(100) NOT NULL,
    permissions character varying(200) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.roles OWNER TO ksvd42;

--
-- TOC entry 181 (class 1259 OID 33617)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO ksvd42;

--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 181
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- TOC entry 182 (class 1259 OID 33630)
-- Name: skill_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE skill_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_id_seq OWNER TO ksvd42;

--
-- TOC entry 2058 (class 0 OID 0)
-- Dependencies: 182
-- Name: skill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE skill_id_seq OWNED BY pilot_classes.id;


--
-- TOC entry 183 (class 1259 OID 33678)
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: ksvd42
--

CREATE SEQUENCE users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO ksvd42;

--
-- TOC entry 2059 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ksvd42
--

ALTER SEQUENCE users_user_id_seq OWNED BY users.user_id;


--
-- TOC entry 1855 (class 2604 OID 33702)
-- Name: id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilot_classes ALTER COLUMN id SET DEFAULT nextval('skill_id_seq'::regclass);


--
-- TOC entry 1856 (class 2604 OID 33703)
-- Name: pos; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilot_classes ALTER COLUMN pos SET DEFAULT nextval('pilot_classes_order_seq'::regclass);


--
-- TOC entry 1857 (class 2604 OID 33706)
-- Name: id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilot_roles ALTER COLUMN id SET DEFAULT nextval('pilot_roles_id_seq'::regclass);


--
-- TOC entry 1872 (class 2604 OID 33707)
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


--
-- TOC entry 1873 (class 2604 OID 33708)
-- Name: last_name; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN last_name SET DEFAULT ''::character varying;


--
-- TOC entry 1874 (class 2604 OID 33709)
-- Name: first_name; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN first_name SET DEFAULT ''::character varying;


--
-- TOC entry 1875 (class 2604 OID 33710)
-- Name: middle_name; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN middle_name SET DEFAULT ''::character varying;


--
-- TOC entry 1876 (class 2604 OID 33711)
-- Name: password; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN password SET DEFAULT 'a7470858e79c282bc2f6adfd831b132672dfd1224c1e78cbf5bcd057'::character varying;


--
-- TOC entry 1877 (class 2604 OID 33712)
-- Name: rank_id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN rank_id SET DEFAULT (-1);


--
-- TOC entry 1878 (class 2604 OID 33713)
-- Name: rating; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN rating SET DEFAULT 1;


--
-- TOC entry 1879 (class 2604 OID 33714)
-- Name: misc_info; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots ALTER COLUMN misc_info SET DEFAULT ''::text;


--
-- TOC entry 1882 (class 2604 OID 33719)
-- Name: id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY positions ALTER COLUMN id SET DEFAULT nextval('positions_id_seq'::regclass);


--
-- TOC entry 1884 (class 2604 OID 33720)
-- Name: id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY ranks ALTER COLUMN id SET DEFAULT nextval('ranks_id_seq'::regclass);


--
-- TOC entry 1885 (class 2604 OID 33721)
-- Name: pos; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY ranks ALTER COLUMN pos SET DEFAULT nextval('ranks_order_seq'::regclass);


--
-- TOC entry 1887 (class 2604 OID 33722)
-- Name: id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- TOC entry 1865 (class 2604 OID 33731)
-- Name: user_id; Type: DEFAULT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


--
-- TOC entry 2029 (class 0 OID 33495)
-- Dependencies: 169
-- Data for Name: pilot_classes; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO pilot_classes (id, pilot_class, pos) VALUES (-1, 'Без класса', 1);


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 170
-- Name: pilot_classes_order_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('pilot_classes_order_seq', 1, true);


--
-- TOC entry 2031 (class 0 OID 33514)
-- Dependencies: 171
-- Data for Name: pilot_roles; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO pilot_roles (id, pilot_id, role_id) VALUES (1, 1, 1);


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 172
-- Name: pilot_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('pilot_roles_id_seq', 1, true);


--
-- TOC entry 2034 (class 0 OID 33535)
-- Dependencies: 174
-- Data for Name: pilots; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO pilots (user_id, last_name, first_name, middle_name, login, password, rank_id, rating, misc_info, callsign, pilot_class_id, meteomin_day, meteomin_night, last_flight_day, last_flight_night, familiar_planes, division) VALUES (1, 'Воеводин', 'Jack', 'Моисеевич', 'admin', 'a7470858e79c282bc2f6adfd831b132672dfd1224c1e78cbf5bcd057', -1, 1, '', '', -1, -1, -1, NULL, NULL, '', '');


--
-- TOC entry 2035 (class 0 OID 33595)
-- Dependencies: 175
-- Data for Name: positions; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (1, 'Руководитель полетами', 1, 'MANAGEMENT1');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (2, 'Помощник РП', 1, 'MANAGEMENT2');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (3, 'Руководитель ближней зоны', 1, 'MANAGEMENT3');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (4, 'Руководитель дальней зоны', 1, 'MANAGEMENT4');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (5, 'Руководитель зоны посадки', 1, 'MANAGEMENT5');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (6, 'Руководитель полетами на полигоне', 1, 'MANAGEMENT6');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (7, 'Помощник РП на полигоне', 1, 'MANAGEMENT7');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (8, 'Руководит. полетами в аэродр. зоне', 1, 'MANAGEMENT8');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (9, 'Старший инженер полетов', 2, 'SUPPORT1');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (10, 'Дежурный синоптик', 2, 'SUPPORT2');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (11, 'Начальник НПСК', 2, 'SUPPORT3');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (12, 'Дежурный по АТО', 2, 'SUPPORT4');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (13, 'Ст.дежурный по связи и РТО', 2, 'SUPPORT5');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (14, 'Ответственный за РЛО', 2, 'SUPPORT6');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (15, 'Дежурный врач (фельдшер)', 2, 'SUPPORT7');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (16, 'Заместитель командира', 3, 'CHECKER1');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (17, 'Заместитель командира по ЛП', 3, 'CHECKER2');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (18, 'Заместитель командира по ИАС', 3, 'CHECKER3');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (19, 'Старший штурман', 3, 'CHECKER4');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (20, 'Начальник ВОТП', 3, 'CHECKER5');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (21, 'Начальник связи и РТО', 3, 'CHECKER6');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (22, 'Начальник СБП', 3, 'CHECKER7');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (23, 'Командир', 4, 'APPROVER');
INSERT INTO positions (id, position_title, position_group_id, position_code) VALUES (24, 'Начальник штаба', 4, 'CHIEF');


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 176
-- Name: positions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('positions_id_seq', 24, true);


--
-- TOC entry 2037 (class 0 OID 33605)
-- Dependencies: 177
-- Data for Name: ranks; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO ranks (id, full_rank, short_rank, pos) VALUES (-1, '', '', 1);


--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 178
-- Name: ranks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('ranks_id_seq', 1, false);


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 179
-- Name: ranks_order_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('ranks_order_seq', 1, true);


--
-- TOC entry 2040 (class 0 OID 33613)
-- Dependencies: 180
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: ksvd42
--

INSERT INTO roles (id, role, permissions) VALUES (1, 'Install_Admin', 'BASIC_POWER');


--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 181
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('roles_id_seq', 1, true);


--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 182
-- Name: skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('skill_id_seq', 1, false);


--
-- TOC entry 2033 (class 0 OID 33522)
-- Dependencies: 173
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ksvd42
--



--
-- TOC entry 2067 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ksvd42
--

SELECT pg_catalog.setval('users_user_id_seq', 1, true);


--
-- TOC entry 1893 (class 2606 OID 33759)
-- Name: pilot_roles_pilot_id_role_id_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilot_roles
    ADD CONSTRAINT pilot_roles_pilot_id_role_id_key UNIQUE (pilot_id, role_id);


--
-- TOC entry 1895 (class 2606 OID 33761)
-- Name: pilot_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilot_roles
    ADD CONSTRAINT pilot_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 1889 (class 2606 OID 33763)
-- Name: pilot_skill_title_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilot_classes
    ADD CONSTRAINT pilot_skill_title_key UNIQUE (pilot_class);


--
-- TOC entry 1901 (class 2606 OID 33767)
-- Name: pilots_callsign_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilots
    ADD CONSTRAINT pilots_callsign_key UNIQUE (callsign);


--
-- TOC entry 1903 (class 2606 OID 33769)
-- Name: pilots_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilots
    ADD CONSTRAINT pilots_pkey PRIMARY KEY (user_id, callsign);


--
-- TOC entry 1905 (class 2606 OID 33771)
-- Name: pilots_user_id_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilots
    ADD CONSTRAINT pilots_user_id_key UNIQUE (user_id);


--
-- TOC entry 1907 (class 2606 OID 33789)
-- Name: positions_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);


--
-- TOC entry 1909 (class 2606 OID 33791)
-- Name: positions_position_code_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY positions
    ADD CONSTRAINT positions_position_code_key UNIQUE (position_code);


--
-- TOC entry 1911 (class 2606 OID 33793)
-- Name: ranks_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY ranks
    ADD CONSTRAINT ranks_pkey PRIMARY KEY (id);


--
-- TOC entry 1913 (class 2606 OID 33795)
-- Name: ranks_title_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY ranks
    ADD CONSTRAINT ranks_title_key UNIQUE (full_rank);


--
-- TOC entry 1915 (class 2606 OID 33797)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 1917 (class 2606 OID 33799)
-- Name: roles_role_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_role_key UNIQUE (role);


--
-- TOC entry 1891 (class 2606 OID 33805)
-- Name: skill_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY pilot_classes
    ADD CONSTRAINT skill_pkey PRIMARY KEY (id);


--
-- TOC entry 1897 (class 2606 OID 33833)
-- Name: users_login_key; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_login_key UNIQUE (login);


--
-- TOC entry 1899 (class 2606 OID 33835)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: ksvd42; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- TOC entry 1918 (class 2606 OID 33884)
-- Name: pilot_roles_pilot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilot_roles
    ADD CONSTRAINT pilot_roles_pilot_id_fkey FOREIGN KEY (pilot_id) REFERENCES pilots(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1919 (class 2606 OID 33889)
-- Name: pilot_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilot_roles
    ADD CONSTRAINT pilot_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1921 (class 2606 OID 33914)
-- Name: pilots_pilot_class_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots
    ADD CONSTRAINT pilots_pilot_class_id_fkey FOREIGN KEY (pilot_class_id) REFERENCES pilot_classes(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 1922 (class 2606 OID 33919)
-- Name: pilots_rank_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY pilots
    ADD CONSTRAINT pilots_rank_id_fkey FOREIGN KEY (rank_id) REFERENCES ranks(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 1920 (class 2606 OID 33944)
-- Name: users_rank_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ksvd42
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_rank_id_fkey FOREIGN KEY (rank_id) REFERENCES ranks(id) ON UPDATE CASCADE ON DELETE RESTRICT;


-- Completed on 2018-09-19 18:24:42

--
-- PostgreSQL database dump complete
--

