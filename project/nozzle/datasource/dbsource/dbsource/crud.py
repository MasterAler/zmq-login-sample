# -*- coding: utf-8 -*-
"""
   Это модуль dbsource. Считайте, что это слой взаимодействия с базой.
   Задач две -- уменьшить разбухание кода и не дать исключениям завалить сервер.
   Порробуйте пользоваться или модифицировать, прежде чем что-то изобретать.
"""

import postgresql
from postgresql.exceptions import ForeignKeyError
from postgresql.exceptions import UniqueError
from postgresql.types import Array
import json
from dbsource.config import DB_CONNECTION_STRING

# Коды ошибок, отдаваемые в ответ на запросы, все должны быть централизованы и находиться здесь
# Они должны быть в количестве, достаточном для дифференцирования разных случаев

NO_ERROR = 0  # Всё хорошо, успех
INVALID_PARAMETERS_GIVEN = 700  # Неверные параметры запроса, несовпадение передаваемых типов обычно
UNABLE_TO_COMPLETE_OPERATION = 701  # Запрос провален, невозможно выполнить
DELETE_FAILED_POSSIBLY_CONSTRAINED = 702  # внешний ключ в таблице не позоляет удаление из-за связанной записи
INSERT_FAILED_POSSIBLY_CONSTRAINED = 703  # вставка нарушает ограничение таблицы
UPDATE_FAILED_POSSIBLY_CONSTRAINED = 704  # обновление нарушает ограничение таблицы
EMPTY_DATA_RETURNED_POSSIBLY_WRONG_ID = 705  # запрос вернул пустой ответ, хотя не должен по логике
OPERATION_FAILED_CHECK_DATA_CONSISTENCY = 706  # транзакция провалилась

# Этотипы join-ов, чтобы не было опечаток и прочего
INNER_JOIN = "INNER"
LEFT_JOIN = "LEFT"
RIGHT_JOIN = "RIGHT"


def to_json_result(result):
    """ Функция форматирования ответа сервера, используется единая в модулях """
    return json.dumps({"data": result[0], "error": result[1]}, ensure_ascii=False)


def xstr(s):
    """ Приведение к строке, которое правильно обрабатывает None """
    if s is None:
        return ''
    return str(s)


class CRUD(object):
    """ Знакомьтесь, это базовый класс взаимодействия с БД. Нужен для унифицированного написания SQL-запросов.
        Идея в том, что запрос генерируется на основе строк, подстановкой. Да, py-postgresql поддерживает
        биндинг параметров, но параметризовать приходится не только их. В модулях предполагается много однотипного кода,
        главная задач -- устранить ненужное дублирование и унифицировать поведение API.

        CRUD = Create Read Update Delete

        Чтобы пользоваться этим чудом, нужно создать в модуле логики 1 такой объект, стараться вызывать только
        его методы, надеяться на лучшее

        Сейчас можно составить Select, Insert, Update, Delete общего вида. Также есть хитрый множественный Update
        Эти запросы при правильно переданных параметрах вернут результат операции, выполнив её. Используются
        prepared statements, на перспективу и для простоты. В py-postgresql есть другой тип объекта запроса, чуть
        лековеснее, но с этим удобно и хорошо.

        Надо понимать, что запрос может бросить исключение -- кроме SELECT, хотя можно и с ним такого добиться.
        В модулях логики следует передавать объекты рабочих функций (используйте *лямбды* для этого) внутрь
        специальных методов, которые ловят стандартные исключения и возвращают код ошибки. Если появилось новое
        исключение -- добавляйте в обработку. Необработанные исключения **валят сервер**.

        Для передачи join - кусочка в 'выполнятор' запроса, есть метод, который сгенерирует соответствующую строку.
        Статические методы данного класса позволяют форматировать ответ базы, в том числе преобразовывая типы
        py-postgresql в простые. Помните, если это не список или словарь, клиент не сможет разобрать ответ.

        Типовые выполняторы выполняются в транзакции (этот with), если запрос сложный -- инициируйте транзакцию вручную.
    """

    # Это шаблоны запросов, в них тупо производится подстановка
    __select_query = "SELECT %s FROM %s %s WHERE %s %s"
    __insert_query = "INSERT INTO %s %s VALUES %s"
    __delete_query = "DELETE FROM %s WHERE %s"
    __update_query = "UPDATE %s SET %s WHERE %s"
    __join_query_tail = "%s JOIN %s on %s"
    __delete_set_query = "DELETE FROM %s WHERE %s IN (%s)"
    __multi_update_query = "UPDATE %s AS t SET %s FROM (VALUES %s ) AS c(%s) WHERE %s"

    __db_handle = None
    __transaction = None

    def __init__(self):
        """ Это не совсем конструктор, но инициализация происходит тут """
        self.__connection_string = DB_CONNECTION_STRING
        self.reconnect()

    def reconnect(self):
        """ После возникновения исключения *необходимо* переподключиться, сбросив ошибку с соединения """
        self.__db_handle = postgresql.open(self.__connection_string)
        self.__transaction = None

    def begin_transaction(self):
        """ Очевидно, начинает транзакцию. Её можно принять или откатить"""
        self.__transaction = self.__db_handle.xact()
        self.__transaction.begin()

    def commit_transaction(self):
        """ Принять транзакцию """
        self.__transaction.commit()

    def rollback_transaction(self):
        """ Откатить транзакцию """
        self.__transaction.rollback()

    def reset(self):
        """ Перезагружает соединение """
        self.__db_handle.reset()

    def select_sql(self, table_name, fields=None, where=None, join_blocks=None, additional=''):
        """ Выполнятор Select.  Без параметров выберет всю таблицу. Возвращает результат запроса.

            table_name -- имя таблицы
            fields -- список названий полей, по которым выборка. По умолчанию = *
            where -- условие выборки. Либо словарь для подстановок вида поле = значение, либо строка. Не обязателен
            join_blocks -- список строк, в которых записаны условия join, генерируются отдельным методом
            additional -- опциональная строка для LIMIT, ORDER BY и прочего, что не учтено
        """
        if not where: where = {}
        if not fields: fields = ['*']
        join_chain = "" if not join_blocks else " ".join(
            [block % table_name for block in join_blocks if block.find('%s') != -1])
        join_chain2 = "" if not join_blocks else " ".join([block for block in join_blocks if block.find('%s') == -1])

        if isinstance(where, str):
            clauses = where
        else:
            clause_list = ["%s = '%s'" % (key, value) for (key, value) in where.items()]
            clauses = 'True' if not where else " AND ".join(clause_list)

        sql = (CRUD.__select_query % (fields[0] if fields[0] == '*' else ','.join(fields)
                                      , table_name
                                      , join_chain + " " + join_chain2
                                      , clauses
                                      , " " + additional))
        with self.__db_handle.xact():
            return self.__db_handle.prepare(sql)()

    def insert_sql(self, table_name, data, ret=None, strict_escape=True):
        """ Выполнятор Insert. Вставляет запись в таблицу, возвращает первичный ключ вставки, если указано его имя.

            table_name -- имя таблицы, в неё вставляем
            data -- словарь с вставляемыми данными вида поле : значение
            ret -- имя возвращаемого поля, обычно имя первичного ключа таблицы
            strict_escape -- нужно ли экранировать строки. Обычно *нужно*, отключается в исключительных случаях
        """
        field_string = '(%s)' % ','.join(['\"%s\"' % key for key in data.keys()])
        if strict_escape:
            value_string = '(%s)' % ','.join("$quote$%s$quote$" % value for value in data.values())
        else:
            value_string = '(%s)' % ','.join("%s" % value for value in data.values())
        sql = CRUD.__insert_query % (table_name, field_string, value_string)
        with self.__db_handle.xact():
            if ret is not None:
                sql += " RETURNING %s" % ret
                return self.__db_handle.prepare(sql)()[0][0]
            else:
                return self.__db_handle.prepare(sql)()

    def insert_multi_sql(self, table_name, data, ret=None):
        """ Выполнятор Insert, вставляющий много записей. Работает аналогично вставлятору 1 записи.

            table_name -- имя таблицы, куда вставляем
            data  -- вставляемые данные вида поле : [список значений]. Укажете разную длину списков -- получите странное
            ret -- имя поле, значение которого вернется после вставки. Очевидно, данный метод возвращает список
        """
        field_string = '(%s)' % ','.join(['\"%s\"' % key for key in data.keys()])
        value_string = ','.join(
            ["(%s)" % ','.join(["$quote$%s$quote$" % i for i in item]) for item in zip(*data.values())])
        sql = CRUD.__insert_query % (table_name, field_string, value_string)
        with self.__db_handle.xact():
            if ret is not None:
                sql += " RETURNING %s" % ret
                return self.__db_handle.prepare(sql)()[0]
            else:
                return self.__db_handle.prepare(sql)()

    def delete_sql(self, table_name, where):
        """ Выполнятор Delete. Удаляет по условию where. Возвращает успех операции.

            table_name -- имя таблицы
            where -- условие удаления, tuple или dict для условия удаления, объединяются через AND
        """
        if isinstance(where, tuple):
            return self.delete_set_sql(table_name, str(where[0]), [where[1]])
        elif isinstance(where, dict):
            clause_list = ["%s = '%s'" % (key, value) for (key, value) in where.items()]
            clause_str = 'True' if not where else " AND ".join(clause_list)
            sql = CRUD.__delete_query % (table_name, clause_str)
            with self.__db_handle.xact():
                return bool(self.__db_handle.prepare(sql)()[1])
        elif isinstance(where, str):
            sql = CRUD.__delete_query % (table_name, where)
            with self.__db_handle.xact():
                return bool(self.__db_handle.prepare(sql)()[1])
        else:
            raise TypeError("CRUD.delete_sql takes only tuple or dict as 'where' param")

    def delete_set_sql(self, table_name, field, del_list):
        """ Delete, удаляющий набор. Удаляет присутвтвующие в наборе значения поля.

            table_name -- имя таблицы
            field -- поле, по которому проверяется условие
            del_list -- список значений набора, по которому удаление
        """
        sql = CRUD.__delete_set_query % (table_name, field, ','.join(["'%s'" % value for value in del_list]))
        with self.__db_handle.xact():
            return bool(self.__db_handle.prepare(sql)()[1])

    def update_sql(self, table_name, data, where, data_literal=True):
        """ Выполнятор Update. Возвращет успех операции. Будьте осторожны с экранированием

            table_name -- имя таблицы
            data -- данные для обновления, поле : значение
            where -- условие обновления, словарь 'поле : значение' или строка
            data_literal -- признак полного экранирования строк, обязателен для текста и json, может мешать в случае
                            с цифрами, выключать только в случае явной ошибки запроса
        """
        set_format = "%s = $quote$%s$quote$" if data_literal else "%s = %s"
        set_list = [set_format % (key, value) for (key, value) in data.items()]

        if isinstance(where, dict):
            clause_list = ["%s = '%s'" % (key, value) for (key, value) in where.items()]
            clauses = " AND ".join(clause_list)
        elif isinstance(where, str):
            clauses = where
        else:
            raise TypeError("CRUD.update_sql takes only dict or str as 'where' param")

        sql = CRUD.__update_query % (table_name
                                     , ",".join(set_list)
                                     , clauses)

        with self.__db_handle.xact():
            return bool(self.__db_handle.prepare(sql)()[1])

    def update_multi_sql(self, table_name, multi_data, column_types, key_field):
        """ Множественное обновление, для нескольких записей сразу. Аналогично простому, но параметры хитрее

            table_name -- имя таблицы
            multi_data -- список словарей, в которых данные для обновления
            column_types -- типы колонок, для правильного преобразования к ним
            key_field -- имя первичного ключа, без него никак
        """
        if not key_field in multi_data.keys():
            raise TypeError("CRUD.update_multi_sql: no valid key given")
        for column in multi_data:
            if type(multi_data[column]) != list or not column in column_types.keys():
                raise TypeError("CRUD.update_multi_sql: invalid parameters")

            if column != key_field:
                multi_data[column] = ["$quote$%s$quote$" % item for item in multi_data[column]]

        set_list = ["%s = c.%s::%s" % (key, key, column_types[key]) for key in multi_data.keys() if key != key_field]
        value_string = ','.join([str(item) for item in zip(*multi_data.values())]).replace("'$", '$').replace("$'", '$')

        sql = CRUD.__multi_update_query % (table_name,
                                           ','.join(set_list),
                                           value_string,
                                           ','.join(multi_data.keys()),
                                           "c.%s = text(t.%s)" % (key_field, key_field))

        with self.__db_handle.xact():
            return bool(self.__db_handle.prepare(sql)()[1])

    def join_block(self, other_table_name, clause_fields, join_type=INNER_JOIN):
        """ Генератор строки join для запросов выбора. Ничего не делает, возвращает строку.

            other_table_name -- имя таблицы, которую присоединяем
            clause_fields -- поля, по которым join. (поле_левой, поле_правой)
            join_type -- тип join
        """
        if type(clause_fields) != tuple:
            raise TypeError("CRID.join_block: tuple expected in clause_fields")
        return self.__join_query_tail % (
            join_type, other_table_name, "%s" + ".%s = %s.%s" % (clause_fields[0], other_table_name, clause_fields[1]))

    def join_full_block(self, first_table_name, second_table_name, clause_fields, join_type=INNER_JOIN):
        """ Генератор строки join, где явно можно указать имена обеих таблиц. Возвращает строку.

            first_table_name -- левая таблица
            second_table_name -- правая таблицв
            clause_fields -- условие, tuple
            join_type -- тип join
        """
        if type(clause_fields) != tuple:
            raise TypeError("CRID.join_block: tuple expected in clause_fields")
        return self.__join_query_tail % (
            join_type, first_table_name,
            "%s.%s = %s.%s" % (first_table_name, clause_fields[0], second_table_name, clause_fields[1]))

    def add_safe_exec(self, inserter):
        """ Обертка для безопасной вставки записей. Принимает функцию-вставлятор.
            Возвращает кортеж из нового первичного ключа и код ошибки.

            Считаем, что первичный ключ в базе всегда зовут id, -1 = неудачная вставка
        """
        new_id = -1
        errcode = NO_ERROR
        try:
            new_id = inserter()
        except UniqueError as e:
            self.reconnect()
            errcode = INSERT_FAILED_POSSIBLY_CONSTRAINED
            print(e)

        except Exception as e:
            self.reconnect()
            errcode = UNABLE_TO_COMPLETE_OPERATION
            print(e)

        return {"id": new_id}, errcode

    def remove_safe_exec(self, deleter):
        """ Обертка для безопасного удаления записей. Принимает функцию - удалятор
            Возвращает успех операции и код ошибки
        """
        ok = False
        errcode = NO_ERROR
        try:
            ok = deleter()
        except ForeignKeyError as e:
            self.reconnect()
            errcode = DELETE_FAILED_POSSIBLY_CONSTRAINED
            print(e)

        except Exception as e:
            self.reconnect()
            errcode = UNABLE_TO_COMPLETE_OPERATION
            print(e)

        return ok, errcode

    def update_safe_exec(self, updater):
        """ Обертка для безопасного обновления записей. Принимает функцию-обновлятор.
            Возвращает успех операции и код ошибки
        """
        success = False
        errcode = NO_ERROR
        try:
            success = updater()
        except UniqueError as e:
            self.reconnect()
            errcode = UPDATE_FAILED_POSSIBLY_CONSTRAINED
            print(e)

        except Exception as e:
            self.reconnect()
            errcode = UNABLE_TO_COMPLETE_OPERATION
            print(e)

        return success, errcode

    def update_by_diff_exec(self, deleter, inserter, null_update=False):
        """ Convenience обертка для обновления таблицы, производящего замену записей

            deleter -- функция-удалятор старых записей
            inserter -- функция-вставлятор новых записей

            null_update -- признак того, нужно ли реально что-то вставлять. Мб не нужно
        """
        try:
            self.begin_transaction()
            result, errcode = self.remove_safe_exec(deleter)

            if errcode == NO_ERROR:
                if not null_update:
                    ids, errcode = self.add_safe_exec(inserter)
                    result = len(ids) > 0

                    if errcode == NO_ERROR:
                        self.commit_transaction()
                    else:
                        self.rollback_transaction()
                else:
                    self.commit_transaction()
                    result = True
                    errcode = NO_ERROR

        except Exception as e:
            self.rollback_transaction()
            result = False
            errcode = OPERATION_FAILED_CHECK_DATA_CONSISTENCY
            print(e)

        return result, errcode

    @staticmethod
    def nested_select_text(table_name, field, where):
        """ Статический метод, помогающий с вложенными *простыми* select. Возвращает строку для подстановки.

            table_name -- имя таблицы
            field, where -- данные для простого вложенного select
        """
        clause_list = ["%s = '%s'" % (key, value) for (key, value) in where.items()]

        clauses = 'True' if not where else " AND ".join(clause_list)
        return '(' + CRUD.__select_query % (field, table_name, '', clauses, "") + ')'

    @staticmethod
    def row_to_dict(row):
        """ convenience конвертер ряда результата запроса в словарь """
        return dict((key, xstr(row[key]) if type(row[key]) != Array else list(row[key])) for key in row.keys())

    @staticmethod
    def selection_to_list(selection):
        """ convenience конвертер результата, возвращенного выборкой, в список """
        return [dict((key, xstr(row[key]) if type(row[key]) != Array else list(row[key])) for key in row.keys())
                for row in selection]