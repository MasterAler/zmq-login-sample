# -*- coding: utf-8 -*-

from nose.tools import *
import postgresql
import random
import json
from dbsource.crud import CRUD

__connection_string = "pq://postgres@localhost:5432/user_database"
crud = CRUD()


def setup():
    print("SETUP!")

def teardown():
    print("TEAR DOWN!")

# def test_last_query():
#     print(crud.last_query())
#     raise IOError

# def test_basic():
#     assert crud is not None
#     assert "pq://postgres@localhost:5432/user_database" == CRUD._CRUD__connection_string

# def test_db_create():
#     print(crud.create_database())

# def test_shitty_crud_insert():
#     crud.insert_sql("study_questions"
#                     , { "uuid": "0753dddd-d65c-4413-91a1-e6030a7abf01"
#                      , "data" : '{"group": "jk.jkl.", "disciplineid": "AERODYNAMICS", "themeid": "КУРС 06", "uuid": "{de8ade0b-743c-4bac-a3e3-218cc37180ff}", "documents": [], "title": "ЖОПАЖОПА"}' });