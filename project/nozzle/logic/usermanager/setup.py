try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'UCC user manager',
    'author': 'aler',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'My email.',
    'version': '0.5.3',
    'install_requires': ['nose', 'dbsource'],
    'packages': ['usermanager'],
    'scripts': [],
    'name': 'usermanager'
}

setup(**config)