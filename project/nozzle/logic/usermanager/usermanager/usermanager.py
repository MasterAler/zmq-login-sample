# -*- coding: utf-8 -*-

import uuid
import hashlib
import codecs
import warnings

from dbsource.crud import *

ROLLBACK_AFTER_LOST_CONSISTENCY = 103


def pwd_hash(password):
    """То, чем кажется -- хеширование пароля"""
    return hashlib.sha224(codecs.encode(password, 'unicode-escape')).hexdigest()


def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used."""

    def new_func(*args, **kwargs):
        warnings.warn("Call to deprecated function %s." % func.__name__,
                      category=DeprecationWarning)
        return func(*args, **kwargs)

    new_func.__name__ = func.__name__
    new_func.__doc__ = func.__doc__
    new_func.__dict__.update(func.__dict__)
    return new_func


class UserManager(object):
    """ Это модуль, отечающий за пользователей в чистом виде, неспецифицированных.
        Роли и авторизация здесь самое главное
    """
    __user_table = 'users'
    __roles_table = 'roles'
    __ranks_table = 'ranks'

    __user_info_args = ["last_name", "first_name", "middle_name", "login", "user_id", "rank_id",
                        "rating", "misc_info"]

    def __init__(self):
        self.__query_gen = CRUD()

    def user_info(self, user_id):
        """Общая информация о пользователе"""
        user_data = self.__get_user(user_id, UserManager.__user_info_args)
        return CRUD.row_to_dict(user_data), EMPTY_DATA_RETURNED_POSSIBLY_WRONG_ID if not user_data else NO_ERROR

    def get_role_list(self):
        """Получает полный список ролей"""
        return CRUD.selection_to_list(self.__query_gen.select_sql(UserManager.__roles_table)), NO_ERROR

    def get_permission_list(self):
        """Получает полный список прав"""
        selection = self.__query_gen.select_sql(UserManager.__roles_table, ["permissions"])
        result = []
        for row in selection:
            result.extend([value for value in row["permissions"].split(";") if value != ''])
        return list(set(result)), NO_ERROR

    def add_role(self, role, permissions):
        """Добавляет роль

            role -- имя роли
            permissions -- права через ';' или их список
        """
        permission_str = ';'.join(permissions) if type(permissions) is list else permissions
        inserter = lambda: self.__query_gen.insert_sql(UserManager.__roles_table,
                                                       {"role": role, "permissions": permission_str}, "id")
        return self.__query_gen.add_safe_exec(inserter)

    def remove_role(self, role):
        """Удаляет роль, принимает её имя"""
        deleter = lambda: self.__query_gen.delete_sql(UserManager.__roles_table, ("role", role))
        return self.__query_gen.remove_safe_exec(deleter)

    def remove_roles(self, roles):
        """Удаляет роли списком, принимает список их имен"""
        if len(roles) == 0:
            return True, NO_ERROR
        else:
            deleter = lambda: self.__query_gen.delete_set_sql(UserManager.__roles_table, 'role', roles)
            return self.__query_gen.remove_safe_exec(deleter)

    def set_role_permissions(self, role, permissions):
        """Устанавливает права данной роли, права списком или строкой через ';' """
        if type(permissions) == list:
            permissions == ';'.join(permissions)
        updater = lambda: self.__query_gen.update_sql(UserManager.__roles_table, {"permissions": permissions},
                                                      {"role": role})
        return self.__query_gen.update_safe_exec(updater)

    def update_role(self, role, new_name, permissions):
        """Обновляет информацию о роли

            role - старое имя роли
            new_name -- новое имя роли
            permissions -- права роли
        """
        if type(permissions) == list:
            permissions == ';'.join(permissions)

        self.__query_gen.begin_transaction()

        ok = True
        ok = ok and self.__query_gen.update_sql(UserManager.__roles_table
                                                , {"role": new_name, "permissions": permissions}
                                                , {"role": role})

        if role != new_name:
            ok = ok and \
                 self.__query_gen.update_sql(UserManager.__roles_table
                                             , {"permissions": "replace(permissions, '%s', '%s')" % (role, new_name)}
                                             , {"1": 1}, False)

        if ok:
            self.__query_gen.commit_transaction()
        else:
            self.__query_gen.rollback_transaction()

        return ok, NO_ERROR if ok else ROLLBACK_AFTER_LOST_CONSISTENCY

    def update_role_set(self, role_set):
        """Сразу обновляет набор ролей, принимает словарь роль : [права]"""
        self.__query_gen.begin_transaction()

        ok = True
        for role in role_set:
            permission_str = ';'.join(role_set[role]) if type(role_set[role]) is list else role_set[role]
            ok = ok and self.__query_gen.update_sql(UserManager.__roles_table
                                                    , {"permissions": permission_str}
                                                    , {"role": role})

        if ok:
            self.__query_gen.commit_transaction()
        else:
            self.__query_gen.rollback_transaction()

        return ok, NO_ERROR if ok else ROLLBACK_AFTER_LOST_CONSISTENCY

    def role_permissions(self, role):
        """Возвращает права данной роли"""
        return self.__query_gen.select_sql(UserManager.__roles_table, ["permissions"], {"role": role})[0][0], NO_ERROR

    @deprecated
    def add_user(self, last_name, first_name, middle_name, password='', misc_info=''):
        """Добавляет пользователя, не пользуйтесь этим методом для чего-то, кроме отладки"""
        login = str(uuid.uuid4())
        new_id = -1
        try:
            new_id = self.__query_gen.insert_sql(UserManager.__user_table, {
                "first_name": first_name,
                "last_name": last_name,
                "middle_name": middle_name,
                "login": login,
                "password": pwd_hash(password),
                "misc_info": misc_info
            }, "user_id")
        except Exception as e:
            print(e)

        return {"user_id": new_id}, UNABLE_TO_COMPLETE_OPERATION if new_id == -1 else NO_ERROR

    @deprecated
    def remove_user(self, user_id):
        """Удаляет пользователя, нужна только для отладки"""
        return self.__query_gen.delete_sql(UserManager.__user_table, ("user_id", user_id)), NO_ERROR

    def get_user_rating(self, user_id):
        """Получает рейтинг (оценку) пользователя. Нужна только пока не доделана система оценок"""
        user_data = self.__get_user(user_id, ["rating"])
        user_rating = user_data["rating"]
        return {"rating": user_rating} if user_rating else {}, \
               EMPTY_DATA_RETURNED_POSSIBLY_WRONG_ID if not user_rating else NO_ERROR

    def set_user_rating(self, user_id, rating):
        """Устанавливает оценку пользователя"""
        return self.__set_user_attr(user_id, "rating", rating)

    def get_user_list(self):
        """Получает список пользователей"""
        return CRUD.selection_to_list(
            self.__query_gen.select_sql(UserManager.__user_table, UserManager.__user_info_args)), NO_ERROR

    def auth_user(self, login, password):
        """Проверяет авторизацию пользователя, принимает логин и пароль. Возвращает user_id в случае успеха"""
        id_data = self.__query_gen.select_sql(UserManager.__user_table, ["user_id"],
                                              {"login": login, "password": pwd_hash(password)})

        return str(id_data[0][0]) if id_data else '', NO_ERROR

    def reset_user_password(self, user_id, password_string):
        """Сбрасывает пароль пользователя, заменяя на password_string"""
        return self.__set_user_attr(user_id, "password", pwd_hash(password_string))

    # private api, for internal use only
    def __get_user(self, user_id, args):
        data = self.__query_gen.select_sql(UserManager.__user_table, args, {"user_id": user_id})
        return data[0] if data else {}

    def __set_user_attr(self, user_id, field, value):
        updater = lambda: self.__query_gen.update_sql(UserManager.__user_table, {field: value}, {"user_id": user_id})
        return self.__query_gen.update_safe_exec(updater)

    def __set_user_attrs(self, user_id, user_data):
        updater = lambda: self.__query_gen.update_sql(UserManager.__user_table, user_data, {"user_id": user_id})
        return self.__query_gen.update_safe_exec(updater)
