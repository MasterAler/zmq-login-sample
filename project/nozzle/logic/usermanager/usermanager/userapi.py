# -*- coding: utf-8 -*-

from dbsource.crud import to_json_result
from usermanager.usermanager import UserManager

# N.B.: I hardcoded connection string in CRUD until db interactions are thoroughly upgraded
db = UserManager()


def reset_user_password(user_id, password_string):
    return to_json_result(db.reset_user_password(user_id, password_string))


def add_user(last_name, first_name, middle_name, password, role_list=''):
    return to_json_result(db.add_user(last_name, first_name, middle_name, password, role_list))


def add_role(role, permissions=''):
    return to_json_result(db.add_role(role, permissions))


handlers = {
    'get_user_list': lambda: to_json_result(db.get_user_list()),

    'authenticate_user': lambda login, password: to_json_result(db.auth_user(login, password)),
    'reset_user_password': reset_user_password,

    'get_user_rating': lambda user_id: to_json_result(db.get_user_rating(user_id)),
    'set_user_rating': lambda user_id, rating: to_json_result(db.set_user_rating(user_id, rating)),
    'update_user_rating': lambda user_id, rating: to_json_result(
        db.set_user_rating(user_id, db.get_user_rating(user_id) + rating)),

    'add_role': add_role,
    'remove_role': lambda role: to_json_result(db.remove_role(role)),
    'remove_roles': lambda roles: to_json_result(db.remove_roles(roles)),
    'set_role_permissions': lambda role, permissions: to_json_result(db.set_role_permissions(role, permissions)),
    'update_role': lambda role, new_name, permissions: to_json_result(db.update_role(role, new_name, permissions)),
    'get_role_permissions': lambda role: to_json_result(db.role_permissions(role)),
    'get_role_list': lambda: to_json_result(db.get_role_list()),
    'update_role_list': lambda role_set: to_json_result(db.update_role_set(role_set)),
    'get_permission_list': lambda: to_json_result(db.get_permission_list())
}

