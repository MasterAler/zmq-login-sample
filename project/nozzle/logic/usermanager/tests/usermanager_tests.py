# -*- coding: utf-8 -*-

from nose.tools import *
import random
import json
import uuid
from usermanager.usermanager import UserManager

NO_ERROR = 0
manager = UserManager()


def test_user_crud():
    result = manager.add_user("Пенкин", "Василий", "Степанович", "111")[0]
    uid = result["user_id"]

    upd = manager.set_user_rating(uid, 6)
    assert upd[1] == NO_ERROR

    user_reply = manager.user_info(uid)
    assert user_reply[1] == NO_ERROR
    assert manager.user_info(-42)[1] != NO_ERROR

    delete_success = manager.remove_user(uid)[1]
    assert delete_success == NO_ERROR


def test_user_list():
    result = manager.get_user_list()
    assert result[1] == NO_ERROR
    print(result)


def test_user_auth():
    pwd = str(int(random.random() * 100000))
    result = manager.add_user("Пенкин", "Василий", "Степанович", pwd)
    uid = result[0]["user_id"]

    login = manager.user_info(uid)[0]["login"]
    assert manager.auth_user(login, pwd)[1] == NO_ERROR
    assert not manager.auth_user(login, str(int(random.random() * 100000)))[0]

    delete_success = manager.remove_user(uid)[1]
    assert delete_success == NO_ERROR


def test_role_crud():
    role_name = "РОЛЬ"

    result = manager.add_role(role_name, "read;write;delete")
    new_id = result[0]["id"]
    error = result[1]
    assert error == NO_ERROR
    assert new_id >= 0

    upd = manager.set_role_permissions(role_name, "something;something_else")
    assert upd[1] == NO_ERROR

    new_name = "ХУИТА"
    upd = manager.update_role(role_name, new_name, "something;something_else")
    assert upd[1] == NO_ERROR

    delete_success = manager.remove_role(new_name)[1]
    assert delete_success == NO_ERROR

    result = manager.add_role("РОЛЬ1", "read;write;delete")
    new_id = result[0]["id"]
    error = result[1]
    assert error == NO_ERROR
    assert new_id >= 0

    result = manager.add_role("РОЛЬ2", "read;write;delete")
    new_id = result[0]["id"]
    error = result[1]
    assert error == NO_ERROR
    assert new_id >= 0

    result = manager.update_role_set({'РОЛЬ1': "read;write", 'РОЛЬ2': "write;delete"})
    assert result[1] == NO_ERROR

    delete_success = manager.remove_roles(['РОЛЬ1', 'РОЛЬ2'])[1]
    assert delete_success == NO_ERROR


def test_list_getters():
    result = manager.get_user_list()
    assert result[1] == NO_ERROR
    # print(result)

    result = manager.get_role_list()
    assert result[1] == NO_ERROR
    # # print(result["data"])

    result = manager.get_permission_list()
    assert result[1] == NO_ERROR
    # print(result[0])
